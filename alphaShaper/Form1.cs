﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using C1.Win.C1Chart;
using System.IO;

namespace alphaShaper
{
    public partial class Form1 : Form
    {

        const int SEQUENCE_LINE_COLOR_R = 122;
        const int SEQUENCE_LINE_COLOR_G = 122;
        const int SEQUENCE_LINE_COLOR_B = 122;
        const int SEQUENCE_SYMBOL_COLOR_R = 122;
        const int SEQUENCE_SYMBOL_COLOR_G = 122;
        const int SEQUENCE_SYMBOL_COLOR_B = 122;
        const int ALPHA_VALUE_SMALL = 2;
        const int ALPHA_VALUE_MIDDLE = 50;
        const int ALPHA_VALUE_LARGE = 500;
        const int ALPHA_VALUE_CONVEX = 1000000;



        private string path, fileName;
        private string xLabel, yLabel, startDate, endDate, threshold;
        private FileInfo xFi;    
        private StreamReader valSr;
        private StreamReader seqSr;

        private int initialVertex;
        private int alphaValue = 1;
        private double area;

        /**xValList = 각 점의 X좌표 리스트
         * yValList = 각 점의 Y좌표 리스트
         * prevPointList = 그리는 pair의 시작 점
         * postPointList = 그리는 pair의 끝 점
         * */

        private List<double> xValList = new List<double>();
        private List<double> yValList = new List<double>();
        private List<int> prevPointList = new List<int>();
        private List<int> postPointList = new List<int>();

        public Form1()
        {
            InitializeComponent();
        }

        private void ribbonButton1_Click(object sender, EventArgs e)
        {
            xValList.Clear();
            yValList.Clear();

            OpenFileDialog OF = new OpenFileDialog();
            OF.DefaultExt = "*.xls";
            OF.Filter = "텍스트(*.txt)|*.txt|모든 파일(*.*)|*.*";
            OF.FilterIndex = 1;
            string[] tmpFileName, tmpxLabel, tmpThreTxt;
            if (OF.ShowDialog() == DialogResult.OK)
            {
                path = OF.FileName;

                tmpFileName = path.Split('\\');
                fileName = tmpFileName[tmpFileName.Length - 1];

                tmpFileName = path.Split('_');
                
                tmpxLabel = tmpFileName[0].Split('\\');
                tmpThreTxt = tmpFileName[4].Split('.');

                xLabel = tmpxLabel[tmpxLabel.Length - 1];                                
                yLabel = tmpFileName[1];
                startDate = tmpFileName[2];
                endDate = tmpFileName[3];

                if (tmpThreTxt.Length > 2)
                {
                    threshold = tmpThreTxt[0] + "." + tmpThreTxt[1];
                }
                else
                {
                    threshold = tmpThreTxt[0];
                }

                startDateTxt.Clear();                
                startDateTxt.AppendText("start date : "  + startDate);
                endDateTxt.Clear();
                endDateTxt.AppendText("end date : " + endDate);
                threTxt.Clear();
                threTxt.AppendText("threshold : " + threshold);
                
                
                xFi = new FileInfo(@path);
                valSr = xFi.OpenText();

                xAxisInfoText.Clear();
                xAxisInfoText.AppendText("x axis data : " + xLabel);

                yAxisInfoText.Clear();
                yAxisInfoText.AppendText("y axis data : " + yLabel);

                loadVertexArray();
            }
        }

        private void ribbonButton2_Click(object sender, EventArgs e)
        {
            OpenFileDialog OF = new OpenFileDialog();
            OF.DefaultExt = "*.xls";
            OF.Filter = "텍스트(*.txt)|*.txt|모든 파일(*.*)|*.*";
            OF.FilterIndex = 1;
            if (OF.ShowDialog() == DialogResult.OK)
            {
                path = OF.FileName;
                xFi = new FileInfo(@path);
                seqSr = xFi.OpenText();
                loadDrawSequenceArray();                
            }
        }

        private void ribbonButton3_Click(object sender, EventArgs e)
        {
            //c1Chart1.ChartArea.AxisX.Max = 450;
            //c1Chart1.ChartArea.AxisX.Min = -450;
            //c1Chart1.ChartArea.AxisY.Max = 450;
            //c1Chart1.ChartArea.AxisY.Min = -450;

            c1Chart1.ChartArea.AxisX.UnitMajor = 100;
            c1Chart1.ChartArea.AxisY.UnitMajor = 100;
            c1Chart1.ChartArea.AxisX.UnitMinor = 50;
            c1Chart1.ChartArea.AxisX.UnitMinor = 50;

            c1Chart1.ChartArea.AxisX.Text = xLabel;
            c1Chart1.ChartArea.AxisY.Text = yLabel;

            areaValue.Clear();
            areaValue.AppendText("결과 값 : " + area);
            drawDataShape();
            drawOnChart();
        }

        private void loadVertexArray()
        {
            string line;
            string[] lineArr;

            xValList.Clear();
            yValList.Clear();
            
            while (valSr.Peek() > 0)
            {
                line = valSr.ReadLine();
                lineArr = line.Split(' ');
                xValList.Add(double.Parse(lineArr[0]));
                yValList.Add(double.Parse(lineArr[1]));
            }
            logText.Clear();
            logText.AppendText("vertext array is loaded");
        }

        private void loadDrawSequenceArray()
        {
            string line;
            string[] lineArr;
            prevPointList.Clear();
            postPointList.Clear();
            int[,] vertexArr = new int[xValList.Count+1,2];
            while (seqSr.Peek() > 0)
            {
                line = seqSr.ReadLine();
                if (!line.Contains('%'))
                {
                    lineArr = line.Split(' ');
                    int prevPoint = int.Parse(lineArr[0]) + 1;
                    int postPoint = int.Parse(lineArr[1]) + 1;
                    prevPointList.Add(prevPoint);
                    postPointList.Add(postPoint);

                    if (initialVertex == 0)
                    {
                        initialVertex = int.Parse(lineArr[0]) + 1;
                    }
                    if (vertexArr[int.Parse(lineArr[0]) + 1, 0] == 0)
                    {
                        vertexArr[int.Parse(lineArr[0]) + 1 , 0] = int.Parse(lineArr[1]) + 1;
                        if (vertexArr[int.Parse(lineArr[1]) + 1, 0] == 0)
                        {
                            vertexArr[int.Parse(lineArr[1]) + 1, 0] = int.Parse(lineArr[0]) + 1;
                        }
                        else
                        {
                            vertexArr[int.Parse(lineArr[1]) + 1, 1] = int.Parse(lineArr[0]) + 1;
                        }
                    }
                    else
                    {
                        vertexArr[int.Parse(lineArr[0]) + 1, 1] = int.Parse(lineArr[1]) + 1;
                        if (vertexArr[int.Parse(lineArr[1]) + 1, 0] == 0)
                        {
                            vertexArr[int.Parse(lineArr[1]) + 1, 0] = int.Parse(lineArr[0]) + 1;
                        }
                        else
                        {
                            vertexArr[int.Parse(lineArr[1]) + 1, 1] = int.Parse(lineArr[0]) + 1;
                        } 
                    }
                }
            }

            calculateArea(vertexArr);

            logText.Clear();
            logText.AppendText("Drawing Sequence is loaded");
        }
        private void drawOnChart()
        {
            int prevPoint, postPoint;
            List<double> xPoint = new List<double>();
            List<double> yPoint = new List<double>();

            c1Chart1.ChartGroups.Group0.ChartData.SeriesList.Clear();
            for (int i = 0; i < prevPointList.Count; i++)
            {              
                prevPoint = prevPointList[i];
                xPoint.Add(xValList[prevPoint-1]);
                yPoint.Add(yValList[prevPoint-1]);

                postPoint = postPointList[i];
                xPoint.Add(xValList[postPoint-1]);
                yPoint.Add(yValList[postPoint-1]);

                drawOnPlot(xPoint, yPoint);
                xPoint.Clear();
                yPoint.Clear();
            }
            logText.Clear();
            logText.AppendText("Drawing complete");
        }
        private void drawOnPlot(List<double> xPoint, List<double> yPoint)
        {
            ChartDataSeries cdataSeries = c1Chart1.ChartGroups.Group0.ChartData.SeriesList.AddNewSeries();
            cdataSeries.LineStyle.Thickness = 1;
            cdataSeries.LineStyle.Color = Color.FromArgb(SEQUENCE_LINE_COLOR_R, SEQUENCE_LINE_COLOR_G, SEQUENCE_LINE_COLOR_B);

            //c1Chart1.ChartArea.AxisX.Text = x.ToString() + "월 데이터";
            //c1Chart1.ChartArea.AxisY.Text = y.ToString() + "월 데이터";

            cdataSeries.SymbolStyle.Shape = SymbolShapeEnum.Box;
            cdataSeries.SymbolStyle.Size = 0;
            cdataSeries.SymbolStyle.Color = Color.FromArgb(SEQUENCE_SYMBOL_COLOR_R, SEQUENCE_SYMBOL_COLOR_G, SEQUENCE_SYMBOL_COLOR_B);

            cdataSeries.X.CopyDataIn(xPoint.ToArray());
            cdataSeries.Y.CopyDataIn(yPoint.ToArray());
            //MessageBox.Show("x value : " + qw.ToString() + "y value : " + er.ToString());
        }

        private void calculateArea(int[,] vertexArr)
        {
            //값 계산, vertex 번호 따라서 순서대로 계산하면 됨
            //예제에서 j = post, i = pre
            int iVertextNum = initialVertex;
            int jVertexNum = vertexArr[initialVertex, 1];
            area = 0;
            for (int i = 0; i < prevPointList.Count; i++)
            {
                area = area + (xValList[iVertextNum-1] + xValList[jVertexNum-1]) * (yValList[iVertextNum-1] - yValList[jVertexNum-1]);
                if (vertexArr[iVertextNum, 0] != jVertexNum)
                {
                    jVertexNum = iVertextNum;
                    iVertextNum = vertexArr[iVertextNum, 0];
                }
                else
                {
                    jVertexNum = iVertextNum;
                    iVertextNum = vertexArr[iVertextNum, 1];
                }
            }
            area = area / 2;
            area = area * area;
            area = Math.Sqrt(area);
        }

        private void ribbonButton4_Click(object sender, EventArgs e)
        {
            c1Chart1.ChartGroups.Group0.ChartData.SeriesList.Clear();
        }

        private void alpha2Btn_Click(object sender, EventArgs e)
        {
            generateShapeSequence(ALPHA_VALUE_SMALL);
        }

        private void alpha100Btn_Click(object sender, EventArgs e)
        {
            generateShapeSequence(ALPHA_VALUE_MIDDLE);
        }

        private void alpha500Btn_Click(object sender, EventArgs e)
        {
            generateShapeSequence(ALPHA_VALUE_LARGE);
        }

        private void convextBtn_Click(object sender, EventArgs e)
        {
            generateShapeSequence(ALPHA_VALUE_CONVEX);
        }
        private void generateShapeSequence(int alphaValue)
        {
            this.alphaValue = alphaValue;

            System.Diagnostics.Process process = new System.Diagnostics.Process();
            System.Diagnostics.ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
            startInfo.FileName = "cmd.exe";
            startInfo.WorkingDirectory = xFi.Directory.ToString();
            
            startInfo.UseShellExecute = false;
            startInfo.RedirectStandardInput = true;
            startInfo.RedirectStandardOutput = true;
            startInfo.RedirectStandardError = true;

            process.EnableRaisingEvents = false;
            process.StartInfo = startInfo;
            process.Start();    //프로세스 시작
            string command = "hull -A -aa" + this.alphaValue.ToString() + " -oN -i" + fileName + " -oF" + fileName + alphaValue.ToString() + Environment.NewLine;
            process.StandardInput.Write(command);
            process.StandardInput.Close();

            string result = process.StandardOutput.ReadToEnd();                     //실행결과를 standard output으로 받아와 string값에 저장
            string error = process.StandardError.ReadToEnd();                        //오류유무를 standard output으로 받아와 string값에 저장
            System.Text.StringBuilder sb = new System.Text.StringBuilder();
            sb.Append("[ Result Info ]");                                                  //출력
            sb.Append(result);
            sb.Append("[ Error Info ]");
            sb.Append(error);

            process.WaitForExit();
            process.Close();      

            xFi = new FileInfo(@path + alphaValue.ToString() + "-alf");
            seqSr = xFi.OpenText();
            loadDrawSequenceArray();
        }

        private void drawDataShapeBtn_Click(object sender, EventArgs e)
        {
            drawDataShape();
        }


        private void drawDataShape()
        {
            drawOnPlot2(xValList, yValList);
        }
        private void drawOnPlot2(List<double> xPoint, List<double> yPoint)
        {
            ChartDataSeries cdataSeries = c1Chart1.ChartGroups.Group0.ChartData.SeriesList.AddNewSeries();
            cdataSeries.LineStyle.Thickness = 1;
            cdataSeries.LineStyle.Color = Color.FromArgb(255, 1, 1);

            cdataSeries.SymbolStyle.Shape = SymbolShapeEnum.Box;
            cdataSeries.SymbolStyle.Size = 0;
            cdataSeries.SymbolStyle.Color = Color.FromArgb(SEQUENCE_SYMBOL_COLOR_R, SEQUENCE_SYMBOL_COLOR_G, SEQUENCE_SYMBOL_COLOR_B);

            cdataSeries.X.CopyDataIn(xPoint.ToArray());
            cdataSeries.Y.CopyDataIn(yPoint.ToArray());
        }

        private void ribbonButton3_Click_1(object sender, EventArgs e)
        {
            c1Chart1.SaveImage(path + alphaValue.ToString()+".png" , System.Drawing.Imaging.ImageFormat.Png);
        }
    }
}
