﻿namespace alphaShaper
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다.
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.c1Ribbon1 = new C1.Win.C1Ribbon.C1Ribbon();
            this.ribbonApplicationMenu1 = new C1.Win.C1Ribbon.RibbonApplicationMenu();
            this.ribbonConfigToolBar1 = new C1.Win.C1Ribbon.RibbonConfigToolBar();
            this.ribbonQat1 = new C1.Win.C1Ribbon.RibbonQat();
            this.ribbonTab1 = new C1.Win.C1Ribbon.RibbonTab();
            this.ribbonGroup1 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton1 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup2 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton2 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup3 = new C1.Win.C1Ribbon.RibbonGroup();
            this.drawAlphaShapeBtn = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup4 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton4 = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup5 = new C1.Win.C1Ribbon.RibbonGroup();
            this.alpha2Btn = new C1.Win.C1Ribbon.RibbonButton();
            this.alpha100Btn = new C1.Win.C1Ribbon.RibbonButton();
            this.alpha500Btn = new C1.Win.C1Ribbon.RibbonButton();
            this.convextBtn = new C1.Win.C1Ribbon.RibbonButton();
            this.ribbonGroup6 = new C1.Win.C1Ribbon.RibbonGroup();
            this.xAxisInfoText = new System.Windows.Forms.RichTextBox();
            this.yAxisInfoText = new System.Windows.Forms.RichTextBox();
            this.startDateTxt = new System.Windows.Forms.RichTextBox();
            this.endDateTxt = new System.Windows.Forms.RichTextBox();
            this.areaValue = new System.Windows.Forms.RichTextBox();
            this.logText = new System.Windows.Forms.RichTextBox();
            this.c1Chart1 = new C1.Win.C1Chart.C1Chart();
            this.drawDataShapeBtn = new C1.Win.C1Ribbon.RibbonButton();
            this.threTxt = new System.Windows.Forms.RichTextBox();
            this.richTextBox2 = new System.Windows.Forms.RichTextBox();
            this.ribbonGroup7 = new C1.Win.C1Ribbon.RibbonGroup();
            this.ribbonButton3 = new C1.Win.C1Ribbon.RibbonButton();
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Chart1)).BeginInit();
            this.SuspendLayout();
            // 
            // c1Ribbon1
            // 
            this.c1Ribbon1.ApplicationMenuHolder = this.ribbonApplicationMenu1;
            this.c1Ribbon1.ConfigToolBarHolder = this.ribbonConfigToolBar1;
            this.c1Ribbon1.Location = new System.Drawing.Point(0, 0);
            this.c1Ribbon1.Name = "c1Ribbon1";
            this.c1Ribbon1.QatHolder = this.ribbonQat1;
            this.c1Ribbon1.Size = new System.Drawing.Size(1058, 156);
            this.c1Ribbon1.Tabs.Add(this.ribbonTab1);
            // 
            // ribbonApplicationMenu1
            // 
            this.ribbonApplicationMenu1.Name = "ribbonApplicationMenu1";
            // 
            // ribbonConfigToolBar1
            // 
            this.ribbonConfigToolBar1.Name = "ribbonConfigToolBar1";
            // 
            // ribbonQat1
            // 
            this.ribbonQat1.Name = "ribbonQat1";
            // 
            // ribbonTab1
            // 
            this.ribbonTab1.Groups.Add(this.ribbonGroup1);
            this.ribbonTab1.Groups.Add(this.ribbonGroup2);
            this.ribbonTab1.Groups.Add(this.ribbonGroup6);
            this.ribbonTab1.Groups.Add(this.ribbonGroup3);
            this.ribbonTab1.Groups.Add(this.ribbonGroup4);
            this.ribbonTab1.Groups.Add(this.ribbonGroup5);
            this.ribbonTab1.Groups.Add(this.ribbonGroup7);
            this.ribbonTab1.Name = "ribbonTab1";
            this.ribbonTab1.Text = "Tab";
            // 
            // ribbonGroup1
            // 
            this.ribbonGroup1.Items.Add(this.ribbonButton1);
            this.ribbonGroup1.Name = "ribbonGroup1";
            this.ribbonGroup1.Text = "dot array open";
            // 
            // ribbonButton1
            // 
            this.ribbonButton1.Name = "ribbonButton1";
            this.ribbonButton1.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton1.SmallImage")));
            this.ribbonButton1.Text = "Button";
            this.ribbonButton1.Click += new System.EventHandler(this.ribbonButton1_Click);
            // 
            // ribbonGroup2
            // 
            this.ribbonGroup2.Items.Add(this.ribbonButton2);
            this.ribbonGroup2.Name = "ribbonGroup2";
            this.ribbonGroup2.Text = "drawing sequence open";
            // 
            // ribbonButton2
            // 
            this.ribbonButton2.Name = "ribbonButton2";
            this.ribbonButton2.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton2.SmallImage")));
            this.ribbonButton2.Text = "Button";
            this.ribbonButton2.Click += new System.EventHandler(this.ribbonButton2_Click);
            // 
            // ribbonGroup3
            // 
            this.ribbonGroup3.Items.Add(this.drawAlphaShapeBtn);
            this.ribbonGroup3.Name = "ribbonGroup3";
            this.ribbonGroup3.Text = "draw alpha shape";
            // 
            // drawAlphaShapeBtn
            // 
            this.drawAlphaShapeBtn.Name = "drawAlphaShapeBtn";
            this.drawAlphaShapeBtn.SmallImage = ((System.Drawing.Image)(resources.GetObject("drawAlphaShapeBtn.SmallImage")));
            this.drawAlphaShapeBtn.Text = "Button";
            this.drawAlphaShapeBtn.Click += new System.EventHandler(this.ribbonButton3_Click);
            // 
            // ribbonGroup4
            // 
            this.ribbonGroup4.Items.Add(this.ribbonButton4);
            this.ribbonGroup4.Name = "ribbonGroup4";
            this.ribbonGroup4.Text = "clear";
            // 
            // ribbonButton4
            // 
            this.ribbonButton4.Name = "ribbonButton4";
            this.ribbonButton4.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton4.SmallImage")));
            this.ribbonButton4.Text = "Button";
            this.ribbonButton4.Click += new System.EventHandler(this.ribbonButton4_Click);
            // 
            // ribbonGroup5
            // 
            this.ribbonGroup5.Items.Add(this.alpha2Btn);
            this.ribbonGroup5.Items.Add(this.alpha100Btn);
            this.ribbonGroup5.Items.Add(this.alpha500Btn);
            this.ribbonGroup5.Items.Add(this.convextBtn);
            this.ribbonGroup5.Name = "ribbonGroup5";
            this.ribbonGroup5.Text = "generate alpha sequence";
            // 
            // alpha2Btn
            // 
            this.alpha2Btn.Name = "alpha2Btn";
            this.alpha2Btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("alpha2Btn.SmallImage")));
            this.alpha2Btn.Text = "2";
            this.alpha2Btn.Click += new System.EventHandler(this.alpha2Btn_Click);
            // 
            // alpha100Btn
            // 
            this.alpha100Btn.Name = "alpha100Btn";
            this.alpha100Btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("alpha100Btn.SmallImage")));
            this.alpha100Btn.Text = "100";
            this.alpha100Btn.Click += new System.EventHandler(this.alpha100Btn_Click);
            // 
            // alpha500Btn
            // 
            this.alpha500Btn.Name = "alpha500Btn";
            this.alpha500Btn.SmallImage = ((System.Drawing.Image)(resources.GetObject("alpha500Btn.SmallImage")));
            this.alpha500Btn.Text = "500";
            this.alpha500Btn.Click += new System.EventHandler(this.alpha500Btn_Click);
            // 
            // convextBtn
            // 
            this.convextBtn.Name = "convextBtn";
            this.convextBtn.SmallImage = ((System.Drawing.Image)(resources.GetObject("convextBtn.SmallImage")));
            this.convextBtn.Text = "convex";
            this.convextBtn.Click += new System.EventHandler(this.convextBtn_Click);
            // 
            // ribbonGroup6
            // 
            this.ribbonGroup6.Items.Add(this.drawDataShapeBtn);
            this.ribbonGroup6.Name = "ribbonGroup6";
            this.ribbonGroup6.Text = "draw data shape";
            // 
            // xAxisInfoText
            // 
            this.xAxisInfoText.Location = new System.Drawing.Point(738, 163);
            this.xAxisInfoText.Name = "xAxisInfoText";
            this.xAxisInfoText.Size = new System.Drawing.Size(308, 43);
            this.xAxisInfoText.TabIndex = 2;
            this.xAxisInfoText.Text = "";
            // 
            // yAxisInfoText
            // 
            this.yAxisInfoText.Location = new System.Drawing.Point(738, 212);
            this.yAxisInfoText.Name = "yAxisInfoText";
            this.yAxisInfoText.Size = new System.Drawing.Size(308, 45);
            this.yAxisInfoText.TabIndex = 3;
            this.yAxisInfoText.Text = "";
            // 
            // startDateTxt
            // 
            this.startDateTxt.Location = new System.Drawing.Point(739, 263);
            this.startDateTxt.Name = "startDateTxt";
            this.startDateTxt.Size = new System.Drawing.Size(308, 43);
            this.startDateTxt.TabIndex = 4;
            this.startDateTxt.Text = "";
            // 
            // endDateTxt
            // 
            this.endDateTxt.Location = new System.Drawing.Point(739, 312);
            this.endDateTxt.Name = "endDateTxt";
            this.endDateTxt.Size = new System.Drawing.Size(308, 43);
            this.endDateTxt.TabIndex = 5;
            this.endDateTxt.Text = "";
            // 
            // areaValue
            // 
            this.areaValue.Location = new System.Drawing.Point(739, 568);
            this.areaValue.Name = "areaValue";
            this.areaValue.Size = new System.Drawing.Size(308, 43);
            this.areaValue.TabIndex = 6;
            this.areaValue.Text = "";
            // 
            // logText
            // 
            this.logText.Location = new System.Drawing.Point(738, 617);
            this.logText.Name = "logText";
            this.logText.Size = new System.Drawing.Size(308, 43);
            this.logText.TabIndex = 7;
            this.logText.Text = "";
            // 
            // c1Chart1
            // 
            this.c1Chart1.Location = new System.Drawing.Point(13, 163);
            this.c1Chart1.Name = "c1Chart1";
            this.c1Chart1.PropBag = resources.GetString("c1Chart1.PropBag");
            this.c1Chart1.Size = new System.Drawing.Size(500, 500);
            this.c1Chart1.TabIndex = 8;
            this.c1Chart1.TabStop = false;
            // 
            // drawDataShapeBtn
            // 
            this.drawDataShapeBtn.Name = "drawDataShapeBtn";
            this.drawDataShapeBtn.SmallImage = ((System.Drawing.Image)(resources.GetObject("drawDataShapeBtn.SmallImage")));
            this.drawDataShapeBtn.Text = "Button";
            this.drawDataShapeBtn.Click += new System.EventHandler(this.drawDataShapeBtn_Click);
            // 
            // threTxt
            // 
            this.threTxt.Location = new System.Drawing.Point(739, 361);
            this.threTxt.Name = "threTxt";
            this.threTxt.Size = new System.Drawing.Size(308, 43);
            this.threTxt.TabIndex = 10;
            this.threTxt.Text = "";
            // 
            // richTextBox2
            // 
            this.richTextBox2.Location = new System.Drawing.Point(739, 410);
            this.richTextBox2.Name = "richTextBox2";
            this.richTextBox2.Size = new System.Drawing.Size(308, 43);
            this.richTextBox2.TabIndex = 11;
            this.richTextBox2.Text = "";
            // 
            // ribbonGroup7
            // 
            this.ribbonGroup7.Items.Add(this.ribbonButton3);
            this.ribbonGroup7.Name = "ribbonGroup7";
            this.ribbonGroup7.Text = "image";
            // 
            // ribbonButton3
            // 
            this.ribbonButton3.Name = "ribbonButton3";
            this.ribbonButton3.SmallImage = ((System.Drawing.Image)(resources.GetObject("ribbonButton3.SmallImage")));
            this.ribbonButton3.Text = "Button";
            this.ribbonButton3.Click += new System.EventHandler(this.ribbonButton3_Click_1);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1058, 673);
            this.Controls.Add(this.richTextBox2);
            this.Controls.Add(this.threTxt);
            this.Controls.Add(this.c1Chart1);
            this.Controls.Add(this.logText);
            this.Controls.Add(this.areaValue);
            this.Controls.Add(this.endDateTxt);
            this.Controls.Add(this.startDateTxt);
            this.Controls.Add(this.yAxisInfoText);
            this.Controls.Add(this.xAxisInfoText);
            this.Controls.Add(this.c1Ribbon1);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.c1Ribbon1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.c1Chart1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private C1.Win.C1Ribbon.C1Ribbon c1Ribbon1;
        private C1.Win.C1Ribbon.RibbonApplicationMenu ribbonApplicationMenu1;
        private C1.Win.C1Ribbon.RibbonConfigToolBar ribbonConfigToolBar1;
        private C1.Win.C1Ribbon.RibbonQat ribbonQat1;
        private C1.Win.C1Ribbon.RibbonTab ribbonTab1;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup1;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton1;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup2;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton2;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup3;
        private C1.Win.C1Ribbon.RibbonButton drawAlphaShapeBtn;
        private System.Windows.Forms.RichTextBox xAxisInfoText;
        private System.Windows.Forms.RichTextBox yAxisInfoText;
        private System.Windows.Forms.RichTextBox startDateTxt;
        private System.Windows.Forms.RichTextBox endDateTxt;
        private System.Windows.Forms.RichTextBox areaValue;
        private System.Windows.Forms.RichTextBox logText;
        private C1.Win.C1Chart.C1Chart c1Chart1;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup4;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton4;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup5;
        private C1.Win.C1Ribbon.RibbonButton alpha100Btn;
        private C1.Win.C1Ribbon.RibbonButton alpha2Btn;
        private C1.Win.C1Ribbon.RibbonButton alpha500Btn;
        private C1.Win.C1Ribbon.RibbonButton convextBtn;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup6;
        private C1.Win.C1Ribbon.RibbonButton drawDataShapeBtn;
        private System.Windows.Forms.RichTextBox threTxt;
        private System.Windows.Forms.RichTextBox richTextBox2;
        private C1.Win.C1Ribbon.RibbonGroup ribbonGroup7;
        private C1.Win.C1Ribbon.RibbonButton ribbonButton3;
    }
}

